using BarsUp.Build;
using BarsUp.Build.MyGet.Tasks;
using BarsUp.Utils;
using Nuke.Common;
using static BarsUp.Build.MyGet.Tasks.MyGetTasks;

partial class Build
{
    [PasswordParameter("Ключ для доступа к API MyGet", Name = "MyGetKey")]
    protected string MyGetKey = "fd1cc3f6-578c-4a8d-8d64-9888db08a8da";
    [StringParameter("Галерея для публикации (MyGet Feed)")]
    protected string MyGetFeed = "rms-projects";
    [Group("Публикация")]
    protected virtual Target Pack => _ => _.Description("Сборка пакетов по всем проектам решения и публикация в корпоративную галерею MyGet").DependsOn(Build).Requires(() => MyGetKey, () => MyGetFeed).Executes(() =>
    {
        PublishToMyGet(s => s.SetBarsUpDefaults().SetFeeds(MyGetFeed).SetApiKey(MyGetKey).SetVersion(SolutionVersion).AddProjects("!!*.demo.*").AddProjects("!!*.*test*.*").AddProjects("!!.build").AddPaths("!!*\\demo\\*").DisableRestore(true).DisablePublish(true));
    }

    );
}