using System;
using System.Diagnostics;
using BarsUp.Build;
using BarsUp.Build.Deploy.Tasks;
using BarsUp.Build.Electron;
using BarsUp.Build.Electron.Tasks;
using BarsUp.Build.Interactive;
using BarsUp.Utils;
using Nuke.Common;
using static Nuke.Common.IO.PathConstruction;

public partial class Build : BarsUpBuild
{
    public Build()
    {
        TerminalHelper.InjectAndClearParameters(this);
    }

    public override int MajorVersion => 3;
    public override int MinorVersion => 0;
    public override string HostProjectName => "BarsUp.WebHost";
    public override AbsolutePath HostProjectDirectory => SolutionDirectory / HostProjectName;
    protected Target Interactive => _ => _.Description("Интерактивная консоль сборщика").Executes(TerminalHelper.RunPersistentTerminal);
    /// <summary>
    /// Сборка Electron-оболочки для хост-приложения на windows
    /// </summary>
    [Group("Публикация")]
    protected virtual Target Electronize => _ => _.Description("Сборка Electron-оболочки для хост-приложения").DependsOn(Restore).Executes(() =>
    {
        Logger.Info("Сборка Electron-оболочки");
        ElectronizeTask.Electronize(s => s.SetBarsUpDefaults().SetTarget(null).DisableRestore(true).DisablePublish(true));
    }

    );
    public static int Main()
    {
        var result = Execute<Build>(x => x.Interactive);
        if (Debugger.IsAttached)
        {
            Logger.Warn("Press Enter to proceed...");
            Console.ReadLine();
        }

        return result;
    }
}