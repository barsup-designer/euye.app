using BarsUp.Build;
using BarsUp.Build.Deploy.Tasks;
using BarsUp.Build.Tasks;
using BarsUp.Utils;
using Nuke.Common;
using static BarsUp.Build.Deploy.Tasks.DeployToIisTask;

partial class Build
{
    [StringParameter("Iis - Целевой сервер")]
    protected string IisServer = "";
    [StringParameter("Iis - Наименование сайта")]
    protected string IisWebSite = "Default Web Site";
    [StringParameter("Iis - Наименование приложения")]
    protected string IisAppName = "";
    [StringParameter("Iis - Имя пользователя")]
    protected string IisUser = "";
    [PasswordParameter("Iis - Пароль")]
    protected string IisPassword;
    [Group("Публикация")]
    protected virtual Target Iis => _ => _.Description("Публикация на сервер Iis").Requires(() => IisServer, () => IisUser, () => IisPassword, () => IisWebSite, () => IisAppName).Executes(() => DeployToIis(s => s.SetBarsUpDefaults().SetSkipRestore(false).SetProfile("Default").SetSite(IisWebSite).SetHost(IisServer).SetUserName(IisUser).SetPassword(IisPassword).SetAppName(IisAppName)));
}